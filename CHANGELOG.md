# Change Log
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/).

## [1.0.0] - 2017-07-07
### Added
- Initial release with the fix for incorrect percentages being shown