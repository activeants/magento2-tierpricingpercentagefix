<?php

namespace ActiveAnts\TierPricingPercentageFix\Pricing\Price;

use Magento\Catalog\Pricing\Price\FinalPrice;
use Magento\Framework\Pricing\Amount\AmountInterface;

class TierPriceFix extends \Magento\Catalog\Pricing\Price\TierPrice
{
    /**
     * Override the getSavePercent function with a corrected function.
     *
     * @param AmountInterface $amount
     * @return float
     */
    public function getSavePercent(AmountInterface $amount)
    {
        $tax = $amount->getAdjustmentAmounts();
        $sumarTax = $tax['tax'];
        return ceil(
            100 - ((100 / $this->priceInfo->getPrice(FinalPrice::PRICE_CODE)->getValue())
                * ($amount->getBaseAmount() + $sumarTax))
        );
    }
}