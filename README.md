# [![Active Ants](http://activeants.nl/content/themes/active-ants/img/logo.png)](http://activeants.nl/)

## Magento 2 tier pricing percentage fix
This module fixes the incorrect percentage for tier price discounts being shown.

##Motivation
We have built a small collection of Dutch Magento 2 shops that use tier pricing but because of a bug in Magento 2 the tier prices show an incorrect percentage, that's where this module comes in.

## Installation
```
composer require activeants/magento2-tierpricingpercentagefix
php bin/magento module:enable ActiveAnts_TierPricingPercentageFix
php bin/magento setup:upgrade
```

## Issues
Feel free to report any issues for this project using the integrated [Bitbucket issue tracker](https://bitbucket.org/activeants/magento2-tierpricingpercentagefix/issues), we will try to get back to issues as fast as possible.

## License
The tier pricing percentage fix is free software, and is released under the terms of the OSL 3.0 license. See LICENSE.md for more information.

## Credits
[Active Ants](http://activeants.nl/) provides e-commerce efulfilment services for webshops. We store products, pick, pack and ship them. But we do much more. With unique efulfilment solutions we make our clients, the webshops, more successful.

~ The [Active Ants](http://activeants.nl/) software development team.
